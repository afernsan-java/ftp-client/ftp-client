
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Abraham Fernandez Sanchez
 */
public final class VistaPrincipalFTP extends javax.swing.JFrame {

    FTPClient cliente = new FTPClient();
    DefaultListModel modelo2;
    JList lista;
    private static String archivoSeleccionado;
    FTPFile[] files;

    public static String getArchivoSeleccionado() {
        return archivoSeleccionado;
    }

    public static void setArchivoSeleccionado(String aArchivoSeleccionado) {
        archivoSeleccionado = aArchivoSeleccionado;
    }

    /**
     * Creates new form VistaPrincipalFTP
     *
     * @throws java.io.IOException excepcion
     */
    public VistaPrincipalFTP() throws IOException {
        initComponents();
        showDirectorio();

    }

    /**
     * Metodo para erstablecer la conexion con el servidor
     * @throws IOException excepcion
     */
    public void AbrirConexionFTP() throws IOException {

        String servFTP = "127.0.0.1";
        System.out.println("Nos conectamos a: " + servFTP);
        String usuario = "usuario1";
        String clave = "usu1";
        jTextFieldNombreUser.setText(usuario);
        //Abre la conexi�n con el servidor FTP indicado en host
        cliente.connect(servFTP);
        //RESPUESTA DEL SERVIDOR TFP = Devuelve el texto completo de la respuesta del servidor FTP 
        boolean login = cliente.login(usuario, clave);
        if (login) {
            jTextFieldAccionRealizada.setText("Login correcto..." + cliente.getReplyString());
            System.out.println("Login correcto..." + cliente.getReplyString());
        } else {
            System.out.println("Login incorrecto...");
            cliente.disconnect();
            System.exit(1);
        }

    }

    /**
     * Metodo para cerrar la conexion con el servidor
     * @throws IOException  excepcion
     */
    public void cerrarConexion() throws IOException {
        boolean logout = cliente.logout();

        if (logout) {
            System.out.println("Logout del servidor FTP...");
            jTextFieldAccionRealizada.setText("Logout del servidor FTP...");
        } else {
            System.out.println("Error al hacer logout...");
            jTextFieldAccionRealizada.setText("Error al hacer logout...");
        }

        //Desconexion del servidor FTP        
        cliente.disconnect();
        jTextFieldAccionRealizada.setText("Desconectado...");
        System.out.println("Desconectado... ");

    }

    /**
     * Metodo para ver o refrescar los directorios y archivos de la vista
     * @throws IOException excepcion
     */
    public void showDirectorio() throws IOException {
        AbrirConexionFTP();
        lista = new JList();
        modelo2 = new DefaultListModel();

        //muestra el directorio actual de trabajo
        jTextFieldDirectorioRaiz.setText("DIRECTORIO RAIZ: " + cliente.printWorkingDirectory());
        jTextFieldDireccionServidor.setText(String.valueOf("Servidor FTP: " + cliente.getLocalAddress()));

        System.out.println("Directorio actual :" + cliente.printWorkingDirectory());
        files = cliente.listFiles();
        System.out.println("Ficheros en el directorio actual:" + files.length);
        jTextFieldAccionRealizada.setText("Ficheros en el directorio actual:" + files.length);

        //array para visualizar el tipo de fichero
        String tipos[] = {"Fichero", "DIR", "Enlace simb."};

        for (int i = 0; i < files.length; i++) {
            // getName() = Devuelve el nombre del fichero 
            // getType() = Devuelve el tipo de fichero, 0 si es un fichero (FILE_TYPE), 1 un directorio (DIRECTORY_TYPE) y 2 un enlace simb�lico (SYMBOLIC_LINK_TYPE) 
            if (!tipos[files[i].getType()].equalsIgnoreCase("Fichero")) {
                modelo2.addElement(tipos[files[i].getType()] + " - " + files[i].getName());
            } else {
                modelo2.addElement(files[i].getName());
            }
            System.out.println("\t" + files[i].getName() + " : " + tipos[files[i].getType()]);
//            modelo2.addElement(tipos[files[i].getType()]+" => "+files[i].getName());            
        }
        jTextFieldInstrucciones.setText("GENERADO ARBOL DE DIRECTORIOS");
        lista.setModel(modelo2);
        jListListaFicheros.setModel(modelo2);
//        cerrarConexion();
    }

    /**
     * Crear carpeta en el servidor
     *
     * @throws IOException excepcion
     */
    public void createCarpeta() throws IOException {
        AbrirConexionFTP();

        String nombreCarpeta = JOptionPane.showInputDialog(this, "Introduce el nombre que le quieres dar a la carpeta");
        //creacion de un nuevo directorio
        String newDirec = nombreCarpeta;
        if (cliente.makeDirectory(newDirec)) {
            cliente.changeWorkingDirectory(newDirec);
            System.out.println("Directorio creado ?");
            jTextFieldInstrucciones.setText("La carpeta " + newDirec + " ha sido creada");
            showDirectorio();
        } else {
            JOptionPane.showMessageDialog(this, "NO SE HA PODIDO CREAR DIRECTORIO");
            System.out.println("NO SE HA PODIDO CREAR DIRECTORIO");
            cerrarConexion();
            showDirectorio();
        }
    }

    /**
     * Borrar carpeta del servidor
     *
     * @throws IOException excepcion
     */
    public void borrarCarpeta() throws IOException {
        String direc = getArchivoSeleccionado();
        String path = cliente.printWorkingDirectory();
        cliente.changeWorkingDirectory(direc);
        if (cliente.removeDirectory(path + direc)) {

            System.out.println("Carpeta eliminada ?");
            jTextFieldAccionRealizada.setText("Carpeta eliminada ?");
            JOptionPane.showMessageDialog(this, "La carpeta ha sido eliminada con exito");
            showDirectorio();
        } else {
            JOptionPane.showMessageDialog(this, "La carpeta No ha sido eliminada");
            jTextFieldAccionRealizada.setText("No se ha podido eliminar la carpeta ?");
            System.out.println("No se ha podido eliminar la carpeta ?");
        }
    }

    /**
     * Borrar archivo del servidor
     *
     * @throws IOException excepcion
     */
    public void borrarArchivo() throws IOException {
        String nomArchivo = getArchivoSeleccionado();
        String path = cliente.printWorkingDirectory();
        cliente.changeWorkingDirectory(path);
        if (cliente.deleteFile(nomArchivo)) {

            System.out.println("Archivo eliminado ?");
            jTextFieldAccionRealizada.setText("Archivo eliminado ?");
            JOptionPane.showMessageDialog(this, "El archivo ha sido eliminado con exito");
            showDirectorio();
        } else {
            JOptionPane.showMessageDialog(this, "El archivo No ha sido eliminado");
            jTextFieldAccionRealizada.setText("No se ha podido eliminar el archivo ?");
            System.out.println("No se ha podido eliminar el archivo ?");
        }
    }

    /**
     * Subir archivo al servidor
     *
     * @throws IOException excepcion
     */
    public void subirArchivoaFTP() throws IOException {

        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

        int result = fileChooser.showOpenDialog(this);
        File fileName = fileChooser.getSelectedFile();

        if (result != JFileChooser.CANCEL_OPTION) {

            if ((fileName == null) || (fileName.getName().equals(""))) {
                setArchivoSeleccionado("...");
            } else {
                setArchivoSeleccionado(fileName.getAbsolutePath());
            }
            //seleccionar el directorio donde se va ha realizar una accion (creacion, borrado de ficheros, etc)
            cliente.changeWorkingDirectory(cliente.printWorkingDirectory());

            //A continuaci�n con el m�todo setFileType() se indica el tipo de fichero a subir. Este tipo es una 
            //constante entera definida en la clase FTP. Se suele pone BINARY_FILE_TYPE que permite enviar ficheros 
            //de cualquier tipo 
            cliente.setFileType(FTPClient.BINARY_FILE_TYPE);

            //Creamos un stream de entrada con los datos del fichero que vamos a subir
            String fileParaSubir = getArchivoSeleccionado();
            BufferedInputStream in = new BufferedInputStream(new FileInputStream(fileParaSubir));

            //se lo pasamos al m�todo storeFile(), en el primer par�metro indicaremos el nombre que tendr� el fichero 
            //en el directorio FTP y en el segundo el InputStrea
            cliente.storeFile(fileName.getName(), in);
            jTextFieldAccionRealizada.setText(getArchivoSeleccionado());
            showDirectorio();
            System.out.println(getArchivoSeleccionado());
        } else {
            showDirectorio();
        }

    }

    /**
     * Metodo para descargar archivos de un servidor a este equipo
     *
     * @throws IOException excepcion
     */
    public void descargarArchivoFTP() throws IOException {
        String archivo = getArchivoSeleccionado();
        String path = cliente.printWorkingDirectory();
        cliente.changeWorkingDirectory(path + archivo);

        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        int result = fileChooser.showOpenDialog(this);
        File fileName = fileChooser.getSelectedFile();

        System.out.println(fileName);
        String nombreFichero = JOptionPane.showInputDialog(this, "Introduce el nombre que le quieres dar al archivo");
        //stream de salida para recibir el fichero descargado 
        BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(fileName + "\\" + nombreFichero));

        if (cliente.retrieveFile(archivo, out)) {

            System.out.println("Recuperado correctamente ?");
            jTextFieldAccionRealizada.setText("Archivo descargado correctamente");
        } else {
            jTextFieldAccionRealizada.setText("No se ha podido descargar ?");
            System.out.println("No se ha podido descargar ?");
        }
        out.close();

    }

    /**
     * Metodo para kla realizacion de una accion determinada cuando en el jList
     * se de dos veces a alguno de sus items
     *
     * @throws IOException excepcion
     */
    public void dobleClickJlist() throws IOException {

        modelo2.clear();
        String direc = getArchivoSeleccionado();
        String path = cliente.printWorkingDirectory();
        cliente.changeWorkingDirectory(path + direc);

        files = cliente.listFiles();
        System.out.println("Ficheros en el directorio actual:" + files.length);
        jTextFieldAccionRealizada.setText("Ficheros en el directorio actual:" + files.length);
        //array para visualizar el tipo de fichero
        String tipos[] = {"Fichero", "DIR", "Enlace simb."};

        for (int i = 0; i < files.length; i++) {
            // getName() = Devuelve el nombre del fichero 
            // getType() = Devuelve el tipo de fichero, 0 si es un fichero (FILE_TYPE), 1 un directorio (DIRECTORY_TYPE) y 2 un enlace simb�lico (SYMBOLIC_LINK_TYPE) 
            if (!tipos[files[i].getType()].equalsIgnoreCase("Fichero")) {
                modelo2.addElement(tipos[files[i].getType()] + " - " + files[i].getName());
            } else {
                modelo2.addElement(files[i].getName());
            }
            System.out.println("\t" + files[i].getName() + " : " + tipos[files[i].getType()]);
//            modelo2.addElement(tipos[files[i].getType()]+" => "+files[i].getName());            
        }
        jTextFieldDirectorioRaiz.setText(path + direc);
        jTextFieldInstrucciones.setText("Esta en la carpeta " + path + direc);
        lista.setModel(modelo2);
        jListListaFicheros.setModel(modelo2);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jTextFieldDireccionServidor = new javax.swing.JTextField();
        jTextFieldDirectorioRaiz = new javax.swing.JTextField();
        jTextFieldNombreUser = new javax.swing.JTextField();
        jButtonDirRaiz = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jButtonSubirFichero = new javax.swing.JButton();
        jButtonDescargarFichero = new javax.swing.JButton();
        jButtonEliminarFichero = new javax.swing.JButton();
        jButtonCrearCarpeta = new javax.swing.JButton();
        jButtonEliminarCarpeta = new javax.swing.JButton();
        jButtonSalir = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jListListaFicheros = new javax.swing.JList<>();
        jPanel4 = new javax.swing.JPanel();
        jTextFieldAccionRealizada = new javax.swing.JTextField();
        jTextFieldInstrucciones = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jTextFieldDireccionServidor.setEditable(false);
        jTextFieldDireccionServidor.setEnabled(false);
        jTextFieldDireccionServidor.setFocusable(false);

        jTextFieldDirectorioRaiz.setEditable(false);
        jTextFieldDirectorioRaiz.setEnabled(false);
        jTextFieldDirectorioRaiz.setFocusable(false);

        jTextFieldNombreUser.setEnabled(false);
        jTextFieldNombreUser.setFocusable(false);

        jButtonDirRaiz.setText("Dir. Raiz");
        jButtonDirRaiz.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDirRaizActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jTextFieldDireccionServidor, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jTextFieldNombreUser, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jTextFieldDirectorioRaiz, javax.swing.GroupLayout.PREFERRED_SIZE, 316, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonDirRaiz)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldDireccionServidor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldNombreUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldDirectorioRaiz, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonDirRaiz, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jButtonSubirFichero.setText("Subir fichero");
        jButtonSubirFichero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSubirFicheroActionPerformed(evt);
            }
        });

        jButtonDescargarFichero.setText("Descargar fichero");
        jButtonDescargarFichero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDescargarFicheroActionPerformed(evt);
            }
        });

        jButtonEliminarFichero.setText("Eliminar fichero");
        jButtonEliminarFichero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEliminarFicheroActionPerformed(evt);
            }
        });

        jButtonCrearCarpeta.setText("Crear carpeta");
        jButtonCrearCarpeta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCrearCarpetaActionPerformed(evt);
            }
        });

        jButtonEliminarCarpeta.setText("Eliminar carpeta");
        jButtonEliminarCarpeta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEliminarCarpetaActionPerformed(evt);
            }
        });

        jButtonSalir.setText("Salir");
        jButtonSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jButtonDescargarFichero, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonEliminarCarpeta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonSalir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonCrearCarpeta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonEliminarFichero, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonSubirFichero, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButtonSubirFichero)
                .addGap(18, 18, 18)
                .addComponent(jButtonDescargarFichero)
                .addGap(18, 18, 18)
                .addComponent(jButtonEliminarFichero)
                .addGap(18, 18, 18)
                .addComponent(jButtonCrearCarpeta)
                .addGap(18, 18, 18)
                .addComponent(jButtonEliminarCarpeta)
                .addGap(18, 18, 18)
                .addComponent(jButtonSalir)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jListListaFicheros.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        jListListaFicheros.setToolTipText("");
        jListListaFicheros.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jListListaFicherosMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jListListaFicheros);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jTextFieldAccionRealizada.setEditable(false);
        jTextFieldAccionRealizada.setEnabled(false);
        jTextFieldAccionRealizada.setFocusable(false);

        jTextFieldInstrucciones.setEditable(false);
        jTextFieldInstrucciones.setEnabled(false);
        jTextFieldInstrucciones.setFocusable(false);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextFieldInstrucciones, javax.swing.GroupLayout.DEFAULT_SIZE, 476, Short.MAX_VALUE)
                    .addComponent(jTextFieldAccionRealizada, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTextFieldInstrucciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextFieldAccionRealizada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalirActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButtonSalirActionPerformed

    private void jButtonCrearCarpetaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCrearCarpetaActionPerformed
        try {
            createCarpeta();
        } catch (IOException ex) {
            Logger.getLogger(VistaPrincipalFTP.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButtonCrearCarpetaActionPerformed

    private void jListListaFicherosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jListListaFicherosMouseClicked

        int ideLista = jListListaFicheros.getSelectedIndex();
        setArchivoSeleccionado(files[ideLista].getName());
        if (evt.getClickCount() == 2) // Se mira si es doble click
        {
            try {
                dobleClickJlist();
            } catch (IOException ex) {
                Logger.getLogger(VistaPrincipalFTP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println(archivoSeleccionado);
    }//GEN-LAST:event_jListListaFicherosMouseClicked

    private void jButtonEliminarCarpetaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEliminarCarpetaActionPerformed
        try {
            borrarCarpeta();
        } catch (IOException ex) {
            Logger.getLogger(VistaPrincipalFTP.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButtonEliminarCarpetaActionPerformed

    private void jButtonEliminarFicheroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEliminarFicheroActionPerformed
        try {
            borrarArchivo();
        } catch (IOException ex) {
            Logger.getLogger(VistaPrincipalFTP.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButtonEliminarFicheroActionPerformed

    private void jButtonDirRaizActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDirRaizActionPerformed
        try {
            showDirectorio();
        } catch (IOException ex) {
            Logger.getLogger(VistaPrincipalFTP.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButtonDirRaizActionPerformed

    private void jButtonSubirFicheroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSubirFicheroActionPerformed
        try {
            subirArchivoaFTP();
        } catch (IOException ex) {
            Logger.getLogger(VistaPrincipalFTP.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_jButtonSubirFicheroActionPerformed

    private void jButtonDescargarFicheroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDescargarFicheroActionPerformed
        try {
            descargarArchivoFTP();
        } catch (IOException ex) {
            Logger.getLogger(VistaPrincipalFTP.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButtonDescargarFicheroActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VistaPrincipalFTP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VistaPrincipalFTP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VistaPrincipalFTP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VistaPrincipalFTP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new VistaPrincipalFTP().setVisible(true);
                } catch (IOException ex) {
                    Logger.getLogger(VistaPrincipalFTP.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCrearCarpeta;
    private javax.swing.JButton jButtonDescargarFichero;
    private javax.swing.JButton jButtonDirRaiz;
    private javax.swing.JButton jButtonEliminarCarpeta;
    private javax.swing.JButton jButtonEliminarFichero;
    private javax.swing.JButton jButtonSalir;
    private javax.swing.JButton jButtonSubirFichero;
    private javax.swing.JList<String> jListListaFicheros;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField jTextFieldAccionRealizada;
    private javax.swing.JTextField jTextFieldDireccionServidor;
    private javax.swing.JTextField jTextFieldDirectorioRaiz;
    private javax.swing.JTextField jTextFieldInstrucciones;
    private javax.swing.JTextField jTextFieldNombreUser;
    // End of variables declaration//GEN-END:variables
}
